import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'

export const usePOSStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()

  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])

  async function getProducts() {
    try {
      loadingStore.doLoad()
      products1.value = (await productService.getProductsbyType(1)).data
      products2.value = (await productService.getProductsbyType(2)).data
      products3.value = (await productService.getProductsbyType(3)).data
      loadingStore.finish()
    } catch (error) {
      console.error('Error fetching products:', error)
      // Handle the error, display a notification, etc.
      loadingStore.finish()
    }
  }
  return { products1, products2, products3, getProducts }
})
