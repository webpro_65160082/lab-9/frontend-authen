import type { Role } from './Role'

type Gender = 'male' | 'female' | 'others'
type User = {
  id?: number
  image: string
  email: string
  password: string
  name: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
}

export type { Gender, User }
